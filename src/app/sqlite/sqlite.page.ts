import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import * as _ from 'lodash';

import { HttpService } from '../services/http.service';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-sqlite',
  templateUrl: './sqlite.page.html',
  styleUrls: ['./sqlite.page.scss'],
})
export class SqlitePage implements OnInit {

  public min: number = 0;
  public max: number = 10;
  public tableName: string = 'PRODUCTS';
  public response: string = '';
  public responseAQ: string = '';

  public longObject: any;

  public database: SQLiteObject;

  public selectSQL: string = `CREATE TABLE IF NOT EXISTS PRODUCTS(skuID bigint(20) DEFAULT NULL, 
    sku varchar(50) DEFAULT NULL, 
    brand varchar(50) DEFAULT NULL, 
    gid tinyint(4) DEFAULT NULL, 
    PID int)`;

  constructor(
    public http: HttpService,
    public sqlite: SQLite,
    public platform: Platform
  ) {
  }

  async ngOnInit() {
    if(this.platform.is('cordova')) {
      this.sqlite.create({
        name: 'ionic412.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        console.log('database created', db);
        this.database = db;
      }).catch((error: any) => {
        console.log(error);
      })
    }
  }

  clear() {
    this.response = this.responseAQ = '';
  }

  async getResponse() {
    let r = await this.http.longAPI().toPromise();
    this.response = 'OK';
    this.longObject = r;

    this.create();
  }

  generateQuery(list: any, type: string, pid: number) {
    return _.map(list, (ob: any, i: any) => {
      let keyString = '', valString = '';
      _.forEach(ob, (val: any, key: any) => { keyString += `${key}, `; valString += `'${val.replace(/'/gm, '')}', `; });
      keyString += 'pid'; valString += `'${pid}'`;
      return `INSERT INTO ${type} (${keyString}) VALUES (${valString});`;
    });
  }

  async create() {
    let tableName = this.tableName;

    await this.database.executeSql(`DROP TABLE IF EXISTS ${tableName}`, []).then((data: any) => {
        console.log(`Table deleted ${tableName}`);
    }).catch((error) => {
      console.log('Error====> delete table', error);
    })
      
    await this.database.executeSql(this.selectSQL, []).then((res: any) => {
      console.log(`Table created ${tableName}`);
    }).catch((error) => {
      console.log('Table creating error ', error);
    })
  }

  async insertRecords(tablesData: any, tableName: any) {
    tablesData = tablesData.slice(this.min, this.max);

    /*
    let insertRows = [];
    if (tableName == "PRODUCTS") {
      let keys: any = Object.keys(tablesData[0])
      let abc = keys.map(i => i).join(',');
      tablesData.map(async (i) => {
      let query = `INSERT INTO ${tableName}(${abc},PID) VALUES(${keys.map((resp: any) => {
      let val = i[`${resp}`];
      let data = `'${val}'`;
      return data;
      }).join(',').concat(', 14')})`;
      
      console.log(query);
      insertRows.push(query)
      })
      */

      let insertRows = this.generateQuery(tablesData, tableName, 14);

      console.log('queries to perform', insertRows);
      
      await this.database.sqlBatch(insertRows).then((res: any) => {
        console.log(`data inseted ${tableName}`);
        this.responseAQ = `data inserted ${tableName}`;
      }).catch((error) => {
        console.log('data insertion error ', error);
        this.responseAQ = `data insertion error ${JSON.stringify(error)}`;
      });
    // }
  }

  async runQuery() {
    this.insertRecords(this.longObject[this.tableName], this.tableName);
  }

}
