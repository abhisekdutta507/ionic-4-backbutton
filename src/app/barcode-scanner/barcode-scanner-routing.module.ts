import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BarcodeScannerPage } from './barcode-scanner.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: BarcodeScannerPage
  },

  {
    path: 'generate',
    loadChildren: '../barcode-generator/barcode-generator.module#BarcodeGeneratorPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarcodeGeneratorRoutingModule { }
