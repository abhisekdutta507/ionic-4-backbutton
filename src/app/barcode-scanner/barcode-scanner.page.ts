import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

import { ActionsService } from '../services/actions.service';

@Component({
  selector: 'app-barcode-scanner',
  templateUrl: './barcode-scanner.page.html',
  styleUrls: ['./barcode-scanner.page.scss'],
})
export class BarcodeScannerPage implements OnInit {

  public json: any;

  public scannerOptions: any = {
    prompt : "Place a code inside the scan area",
    showTorchButton: false,
    disableSuccessBeep: false
  };

  constructor(
    private action: ActionsService,
    private platform: Platform
  ) { }

  ngOnInit() {
  }

  async scanBarCode() {
    if(!this.platform.is('cordova')) { return; }

    this.json = await this.action.scanBarCode(this.scannerOptions);
  }

}
