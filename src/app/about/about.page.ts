import { Component, OnInit } from '@angular/core';

import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  constructor(
    private events: EventsService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.events.bindBackButton();
  }

  ionViewWillLeave() {
    this.events.releaseBackButton(); 
  }

}
