import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { Network } from '@ionic-native/network/ngx';

import { ActionsService } from './actions.service';

let that: any;

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private backButtonAsyncEvent: any;
  
  private networkConnectedStatus: any;
  private networkDisconnectedStatus: any;

  private exit: boolean = false;

  constructor(
    private platform: Platform,
    private action: ActionsService,
    private appMinimize: AppMinimize,
    private network: Network
  ) {
    that = this;
  }

  bindBackButton() {
    // https://stackoverflow.com/questions/51700879/handling-hardware-back-button-in-ionic3-vs-ionic4
    this.backButtonAsyncEvent = this.platform.backButton.subscribe(async (e) => {
      // Exit from ionic application
      // https://forum.ionicframework.com/t/v4-back-button-doesnt-exit-app-solved-tutorial/149994/6

      if (that.exit) { this.appMinimize.minimize(); /* navigator['app'].exitApp(); */ }
      else { this.action.presentNativeToast({message: 'Press again to exit', duration: 1000}); }

      that.exit = true;
      
      setTimeout(() => { that.exit = false; }, 1000);
    });

  }

  releaseBackButton() {
    this.backButtonAsyncEvent.unsubscribe();
  }

  bindNetwork() {
    this.networkConnectedStatus = this.network.onConnect().subscribe(async (e) => {
      console.log('connection', e);
    });

    this.networkDisconnectedStatus = this.network.onDisconnect().subscribe(async (e) => {
      console.log('disconnection', e);
    });
  }

  releaseNetwork() {
    this.networkConnectedStatus.unsubscribe();
    this.networkDisconnectedStatus.unsubscribe();
  }

}
