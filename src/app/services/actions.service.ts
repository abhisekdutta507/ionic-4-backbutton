import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

import { Toast } from '@ionic-native/toast/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  private toast: any;

  constructor(
    private Toast: ToastController,
    private NativeToast: Toast,
    private spinnerDialog: SpinnerDialog,
    private barcodeScanner: BarcodeScanner
  ) { }

  async presentToast(param: any) {
    this.toast = await this.Toast.create(
      Object.assign({position: 'bottom', cssClass: 'toast', mode: 'ios', color: 'dark'}, param)
    );
    this.toast.present();
  }

  async presentNativeToast(params: any) {
    this.toast = await this.NativeToast.showWithOptions(
      Object.assign({position: 'bottom', addPixelsY: -150, duration: 1000}, params)
    ).toPromise();
  }

  async showNativeSpinner(params: any) {
    /**
     * @param title: string data to be appeared at the top of the loader
     * @param message: string data to be appeared at the middle of the loader
     * @param cancelable: boolean. When true can not be closed by touching the overlay
     * @param options: options available on for ios devices
     */
    this.spinnerDialog.show(params.title || '', params.message || 'Loading...', params.cancelable || false, {
      overlayOpacity: 1,
      textColorRed: 0.3,
      textColorGreen: 0.3,
      textColorBlue: 0.3
    });
  }

  async hideNativeSpinner() {
    this.spinnerDialog.hide();
  }

  async scanBarCode(options: any = {}) {
    return new Promise(async (resolve, reject) => {
      let r: any;
      try { r = await this.barcodeScanner.scan(options); }
      catch (e) { reject(e); }
      resolve(r);
    });
  }

  async generateBarCode(type: any, data: any) {
    return new Promise(async (resolve, reject) => {
      let r: any;
      try { r = await this.barcodeScanner.encode(this.barcodeScanner.Encode[type], data); }
      catch (e) { reject(e); }
      resolve(r);
    });
  }

}
