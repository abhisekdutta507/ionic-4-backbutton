import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

// import { StoreService } from './store.service';
// import { PluginService } from './plugin.service';
import { environment } from '../../environments/environment';

let that: any;

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  // private apiurl = environment.production ? 'https://nodeapis101.herokuapp.com/api/v1' : 'http://localhost:5000/api/v1';
  /**
   * @description Select domain available only on Heroku.
   */
  private apiurl = 'http://dev102.developer24x7.com/cnp477/wp-json';

  constructor(
    private http: HttpClient,
    // private store: StoreService,
    // private plugin: PluginService,

    // private transfer: FileTransfer,
  ) {
    that = this;
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      Object.assign(result, { 'error': error.error });
      return of(result as T);
    };
  }

  /**
   * @description Returns HTTP Header Options.
   */
  /*
  option(type: any) {
    return {
      headers: new HttpHeaders(Object.assign({}, type, {
        'Authorization': this.store.getLocalStorage('f-z-token')
      }))
    };
  }
  */

  /**
   * @description Login POST HTTP Request.
   */
  login(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/jwt-auth/v1/token';

    return this.http.post<any>(url, data).pipe(
      tap(message => message),
      catchError(this.handleError('login', {}))
    );
  }

  /**
   * @description
   */
  test(): Observable<HttpResponse<any>> {
    let url = 'https://nodeapis101.herokuapp.com/api/v1/test';

    return this.http.get<any>(url).pipe(
      tap(message => message),
      catchError(this.handleError('test', {}))
    );
  }

  longAPI(): Observable<HttpResponse<any>> {
    let url = 'https://insight3.ultralysis.com/apk/project/index.php?destination=FetchData&action=getdata&deviceID=c84e53f47739b920&apikey=a6b4ed0adaf79d7b7fa7427e98b44d62&FE=RDE11108';

    return this.http.get<any>(url).pipe(
      tap(message => message),
      catchError(this.handleError('long api', {}))
    );
  }

  /*
  signup(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/create-new-user';

    return this.http.post<any>(url, data).pipe(
      tap(message => message),
      catchError(this.handleError('signup', {}))
    );
  }

  category(): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/get-provider-category';

    return this.http.get<any>(url).pipe(
      tap(message => message),
      catchError(this.handleError('user', {}))
    );
  }

  user(): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/get-user-info';

    return this.http.get<any>(url, this.option({})).pipe(
      tap(m => {
        m.data.profile_pic = m.data.profile_pic ? m.data.profile_pic : '/assets/img/avatar.png';
        m.data.ethnicity = m.data.ethnicity.replace(/\n/gm, '').split(',');
        m.data.languages = m.data.languages.replace(/\n/gm, '').split(',');
        m.data.treatment_cycle = this.plugin.unserialize(m.data.treatment_cycle);
        m.data.medicationspast = this.plugin.unserialize(m.data.medicationspast);
        m.data.medications = this.plugin.unserialize(m.data.medications);
        if(m.data.medications.medications) {m.data.medications.medications = m.data.medications.medications.replace(/\,/g, '')}
        if(m.data.medications.optionnotes) {m.data.medications.optionnotes = m.data.medications.optionnotes.replace(/\,/g, '')}
      }),
      catchError(this.handleError('user', {}))
    );
  }

  update(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/update-customer';

    return this.http.post<any>(url, data, this.option({})).pipe(
      tap(message => message),
      catchError(this.handleError('update', {}))
    );
  }

  resetPassword(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/reset-password';

    return this.http.post<any>(url, data).pipe(
      tap(message => message),
      catchError(this.handleError('reset password', {}))
    );
  }

  search(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/search-provider';

    return this.http.post<any>(url, data, this.option({})).pipe(
      tap(message => message),
      catchError(this.handleError('search clinic', {}))
    );
  }
  */

  /*
  async pushFile(path: any, type: string) {
    return new Promise(function(resolve, reject) {
      // let message = 'Welcome to async/await. Input was ' + n;
      // setTimeout(function() {
      //     resolve(message);
      // }, n * 1000);

      let time = new Date().getTime();
      const fileTransfer: FileTransferObject = that.transfer.create();

      let options: FileUploadOptions = {
        fileKey: 'plavatarupload',
        fileName: time + '_avatar.' + type,
        headers: {
          'Authorization': that.store.getLocalStorage('f-z-token')
        },
        params: {
          field_id: 'plavatarupload'
        }
      };
  
      fileTransfer.upload(path, that.apiurl + '/wp-jwt/v1/update-customer-avatar', options)
      .then(async (image) => {
        if(image['responseCode'] === 200) {
          let ir = JSON.parse(image['response']);
          resolve(ir);
        }
        resolve({message: 'Can\'t update now! Please try later.', status: 0});
      }).catch(err => {
        resolve({message: 'Can\'t update now! Please try later.', status: 0});
      });
    });
  }

  clinic(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/get-provider-details';

    return this.http.post<any>(url, data, this.option({})).pipe(
      tap(message => message),
      catchError(this.handleError('clinic', {}))
    );
  }

  a2Fav(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/add-to-favorite';

    return this.http.post<any>(url, data, this.option({})).pipe(
      tap(message => message),
      catchError(this.handleError('a2f', {}))
    );
  }

  rfFav(data: any): Observable<HttpResponse<any>> {
    let url = this.apiurl + '/wp-jwt/v1/remove-from-favorite';

    return this.http.post<any>(url, data, this.option({})).pipe(
      tap(message => message),
      catchError(this.handleError('rff', {}))
    );
  }
  */



}
