import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ActionsService } from '../services/actions.service';

@Component({
  selector: 'app-socket-io-messages',
  templateUrl: './socket-io-messages.page.html',
  styleUrls: ['./socket-io-messages.page.scss'],
})
export class SocketIoMessagesPage implements OnInit {

  constructor(
    private router: Router,
    private action: ActionsService
  ) { }

  ngOnInit() {
  }

  read(item: any) {
    this.action.presentToast({ message: 'This feature is under development', duration: 3000, closeButtonText: 'Dismiss', showCloseButton: true });
  }

  delete(item: any) {
    this.action.presentToast({ message: 'This feature is under development', duration: 3000, closeButtonText: 'Dismiss', showCloseButton: true });
  }

  doRefresh(event: any) {
    setTimeout(() => { event.target.complete(); }, 2000);
  }

}
