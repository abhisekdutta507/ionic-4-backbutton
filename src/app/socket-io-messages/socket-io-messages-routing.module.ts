import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SocketIoMessagesPage } from './socket-io-messages.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SocketIoMessagesPage
  },
  { path: 'texts', loadChildren: '../socket-io-texts/socket-io-texts.module#SocketIoTextsPageModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocketIoMessagesRoutingModule { }
