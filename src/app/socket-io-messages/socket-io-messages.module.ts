import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SocketIoMessagesRoutingModule } from './socket-io-messages-routing.module';

import { SocketIoMessagesPage } from './socket-io-messages.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SocketIoMessagesRoutingModule
  ],
  declarations: [SocketIoMessagesPage]
})
export class SocketIoMessagesPageModule {}
