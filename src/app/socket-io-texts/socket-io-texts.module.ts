import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SocketIoTextsPage } from './socket-io-texts.page';

const routes: Routes = [
  {
    path: '',
    component: SocketIoTextsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SocketIoTextsPage]
})
export class SocketIoTextsPageModule {}
