import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocketIoTextsPage } from './socket-io-texts.page';

describe('SocketIoTextsPage', () => {
  let component: SocketIoTextsPage;
  let fixture: ComponentFixture<SocketIoTextsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocketIoTextsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketIoTextsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
