import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomePage
  },

  {
    path: 'cn-development-api',
    loadChildren: '../cn-development-api/cn-development-api.module#CnDevelopmentAPIPageModule'
  },
  {
    path: 'heroku-development-api',
    loadChildren: '../heroku-development-api/heroku-development-api.module#HerokuDevelopmentAPIPageModule'
  },

  {
    path: 'barcode-scanner',
    loadChildren: '../barcode-scanner/barcode-scanner.module#BarcodeScannerPageModule'
  },
  {
    path: 'calendar',
    loadChildren: '../calendar/calendar.module#CalendarPageModule'
  },
  {
    path: 'date-time-picker',
    loadChildren: '../date-time-picker/date-time-picker.module#DateTimePickerPageModule'
  },
  {
    path: 'spinner',
    loadChildren: '../spinner/spinner.module#SpinnerPageModule'
  },
  {
    path: 'sqlite',
    loadChildren: '../sqlite/sqlite.module#SqlitePageModule'
  },
  {
    path: 'chartjs',
    loadChildren: '../chartjs/chartjs.module#ChartjsPageModule'
  },
  {
    path: 'socketiochat',
    loadChildren: '../socket-io-chat/socket-io-chat.module#SocketIoChatPageModule'
  },
  {
    path: 'notifications',
    loadChildren: '../notifications/notifications.module#NotificationsPageModule'
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
