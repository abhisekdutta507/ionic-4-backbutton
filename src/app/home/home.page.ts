import { Component } from '@angular/core';

import { Calendar } from '@ionic-native/calendar/ngx';
import { DatePicker, DatePickerOptions } from '@ionic-native/date-picker/ngx';

import { HttpService } from '../services/http.service';
import { EventsService } from '../services/events.service';
import { ActionsService } from '../services/actions.service';

let that: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public nodeAPIRes: string = '';
  public cnAPIRes: string = '';
  public calendarRes: string = '';
  public spinnerRes: string = '';
  public datePickerRes: string = '';
  public timePickerRes: string = '';
  public barcodeScannerRes: string = '';

  constructor(
    private http: HttpService,
    private events: EventsService,
    private action: ActionsService,
    private calendar: Calendar,
    private datePicker: DatePicker,
    private timePicker: DatePicker
  ) {
    that = this;
  }

  async testNodeAPI() {
    this.action.showNativeSpinner({ cancelable: true });
    let r = await this.http.test().toPromise();
    this.nodeAPIRes = JSON.stringify(r);
    this.action.hideNativeSpinner();
  }

  async cnLoginAPI() {
    this.action.showNativeSpinner({ cancelable: true });
    let data = new FormData();
    data.append('username', 'abhisek.dutta.507@gmail.com');
    data.append('password', 'abhisek.dutta.507@gmail.com');

    let r = await this.http.login(data).toPromise();
    this.cnAPIRes = JSON.stringify(r);
    this.action.hideNativeSpinner();
  }

  async openCalendar() {
    this.calendar.openCalendar(new Date()).then(msg => {
      this.calendarRes = msg;
    }).catch(err => {
      this.calendarRes = err;
    });
  }

  async showSpinner() {
    this.action.showNativeSpinner({});
  }

  refresh() {
    this.cnAPIRes = this.nodeAPIRes = this.calendarRes = this.spinnerRes = this.datePickerRes = this.timePickerRes = this.barcodeScannerRes = '';
  }

  ionViewDidEnter() {
    this.events.bindBackButton();
  }

  ionViewWillLeave() {
    this.events.releaseBackButton(); 
  }

  public datePickerTheme: string = 'THEME_DEVICE_DEFAULT_LIGHT';
  public datePickerOptions: DatePickerOptions = {
    date: new Date(),
    mode: 'date'
  };

  async showDatePicker() {
    Object.assign(this.datePickerOptions, {
      androidTheme: this.datePicker.ANDROID_THEMES[this.datePickerTheme]
    });
    let r: any;
    try { r = await this.datePicker.show(this.datePickerOptions); }
    catch(e) { this.datePickerRes = e; return; }
    this.datePickerRes = r;
  }

  public timePickerTheme: string = 'THEME_DEVICE_DEFAULT_LIGHT';
  public timePickerOptions: DatePickerOptions = {
    date: new Date(),
    mode: 'time'
  };

  async showTimePicker() {
    Object.assign(this.timePickerOptions, {
      androidTheme: this.timePicker.ANDROID_THEMES[this.timePickerTheme]
    });
    let r: any;
    try { r = await this.timePicker.show(this.timePickerOptions); }
    catch(e) { this.timePickerRes = e; return; }
    this.timePickerRes = r;
  }

  async scanBarCode() {
    let r: any = await this.action.scanBarCode();
    console.log('', r);
  }

  async generateBarCode() {
    // this.action.generateBarCode();
  }

}
