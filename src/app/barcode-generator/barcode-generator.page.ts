import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

import { ActionsService } from '../services/actions.service';

@Component({
  selector: 'app-barcode-generator',
  templateUrl: './barcode-generator.page.html',
  styleUrls: ['./barcode-generator.page.scss'],
})
export class BarcodeGeneratorPage implements OnInit {

  public json: any;
  public encodeType: string = 'TEXT_TYPE';

  constructor(
    private action: ActionsService,
    private platform: Platform
  ) { }

  ngOnInit() {
  }

  async generateBarCode() {
    if(!this.platform.is('cordova')) { return; }

    await this.action.generateBarCode(this.encodeType, this.json);
  }

}
