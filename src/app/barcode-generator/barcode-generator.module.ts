import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BarcodeGeneratorPage } from './barcode-generator.page';

const routes: Routes = [
  {
    path: '',
    component: BarcodeGeneratorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BarcodeGeneratorPage]
})
export class BarcodeGeneratorPageModule {}
