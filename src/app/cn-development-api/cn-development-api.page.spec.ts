import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnDevelopmentAPIPage } from './cn-development-api.page';

describe('CnDevelopmentAPIPage', () => {
  let component: CnDevelopmentAPIPage;
  let fixture: ComponentFixture<CnDevelopmentAPIPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnDevelopmentAPIPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnDevelopmentAPIPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
