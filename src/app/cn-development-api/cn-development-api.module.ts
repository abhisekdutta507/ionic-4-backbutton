import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CnDevelopmentAPIPage } from './cn-development-api.page';

const routes: Routes = [
  {
    path: '',
    component: CnDevelopmentAPIPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CnDevelopmentAPIPage]
})
export class CnDevelopmentAPIPageModule {}
