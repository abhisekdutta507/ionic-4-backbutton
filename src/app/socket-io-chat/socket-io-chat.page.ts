import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActionsService } from '../services/actions.service';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-socket-io-chat',
  templateUrl: './socket-io-chat.page.html',
  styleUrls: ['./socket-io-chat.page.scss'],
})
export class SocketIoChatPage implements OnInit {

  public joinForm: FormGroup;
  public hide = true;

  constructor(
    private form: FormBuilder,
    private router: Router,
    private action: ActionsService,
    private events: EventsService
  ) {
    this.joinForm = this.form.group({
      username: new FormControl('', Validators.compose([])),
      password: new FormControl('', Validators.compose([])),
    });
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.events.bindNetwork();
  }

  ionViewWillLeave() {
    this.events.releaseNetwork(); 
  }

  join() {
    if(!this.joinForm.value.username || !this.joinForm.value.password) { this.action.presentToast({ message: 'Invalid username or password', duration: 3000, closeButtonText: 'Dismiss', showCloseButton: true }); return; }

    this.router.navigate(['/home/socketiochat/messages']);
  }

}
