import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SocketIoChatRoutingModule } from './socket-io-chat-routing.module';

import { SocketIoChatPage } from './socket-io-chat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SocketIoChatRoutingModule
  ],
  declarations: [SocketIoChatPage]
})
export class SocketIoChatPageModule {}
