import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SocketIoChatPage } from './socket-io-chat.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SocketIoChatPage
  },

  { path: 'messages', loadChildren: '../socket-io-messages/socket-io-messages.module#SocketIoMessagesPageModule' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocketIoChatRoutingModule { }
