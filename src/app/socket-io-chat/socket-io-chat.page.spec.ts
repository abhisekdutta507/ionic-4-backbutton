import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocketIoChatPage } from './socket-io-chat.page';

describe('SocketIoChatPage', () => {
  let component: SocketIoChatPage;
  let fixture: ComponentFixture<SocketIoChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocketIoChatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketIoChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
