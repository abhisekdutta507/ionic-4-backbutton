import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HerokuDevelopmentAPIPage } from './heroku-development-api.page';

describe('HerokuDevelopmentAPIPage', () => {
  let component: HerokuDevelopmentAPIPage;
  let fixture: ComponentFixture<HerokuDevelopmentAPIPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HerokuDevelopmentAPIPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HerokuDevelopmentAPIPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
