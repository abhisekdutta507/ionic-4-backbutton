## Basic commands for ionic project

ionic cordova resources

sudo ionic cordova prepare android

sudo ionic cordova run android --prod

sudo ionic cordova build android --prod --release

sudo ionic cordova build ios -- --buildFlag="-UseModernBuildSystem=0"

keytool -genkey -v -keystore security.jks -keyalg RSA -keysize 2048 -validity 10000 -alias ionic4120
Password: 12345678

keytool -importkeystore -srckeystore security.jks -destkeystore security.keystore -deststoretype pkcs12

keytool -exportcert -alias ionic4120 -keystore security.keystore -list -v

keytool -exportcert -alias ionic4120 -keystore security.keystore | openssl sha1 -binary | openssl base64

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore security.keystore ionic4120.apk ionic4120
Password: 1234567890